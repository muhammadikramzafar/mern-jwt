var Cars = require('../models/cars');

exports.get_all_cars = function(req, res, next){
    Cars.find({}, function(err , cars){
        if(err) return next(err);
        res.send(cars);
    })
} 

exports.add_new_car = function (req, res, next){
    console.log(req.body);
    var cars = new Cars({
        name: req.body.name,
        model: req.body.model,
        description: req.body.description
    });
    cars.save(function(err){
        if(err) return next(err)
        res.send('car Added')
    })
}