var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CarSchema = new Schema({
    name: {type:String, required: true, max:100},
    model:{type:Number,required:true,max:99999},
    description:{type:String,required:false,max:1024}
});

module.exports = mongoose.model('Cars', CarSchema);