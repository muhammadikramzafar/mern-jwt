var express = require('express');
var router = express.Router();

var cars_controller = require('../controllers/cars');

router.get('/examples', (req,res,next) => {
    res.render('home',null);
});

router.get('/cars', cars_controller.get_all_cars);

router.post('/car', cars_controller.add_new_car);

module.exports = router;