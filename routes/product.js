var express = require('express');
var router = express.Router();
var auth = require('../middleware/auth');
var Product = require('../models/product');

// Require the controllers WHICH WE DID NOT CREATE YET!!
var product_controller = require('../controllers/product');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', product_controller.test);
router.get('/test2', auth,  async(req, res) => {
    Product.find({} , function(err, product) {
        if (err) return next(err);
        res.send(product);
    })
});

router.post('/create', product_controller.product_create);

router.get('/:id', product_controller.product_details);

router.put('/:id/update', product_controller.product_update);

router.delete('/:id/delete', product_controller.product_delete);


module.exports = router;